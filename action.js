"use strict";

let currentDate = document.getElementById("currentDate");
let startTime = document.getElementById("startTime");
let stopTime = document.getElementById("stopTime");
let minute = document.getElementById("minute");
let currency = document.getElementById("currency");
let button = document.getElementById("btn");
let icon = document.getElementById("icon");

setInterval(() => {
  dateInfo();
}, 1000);

// create date time from object Date()
function dateInfo() {
  let dateDisplay = new Date();
  let date = dateDisplay.toLocaleDateString("en-US", {
    weekday: "short",
    year: "numeric",
    month: "short",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  });
  currentDate.innerHTML = date.replace(",", " ");
}

let startDate;

// start time
const startTimer = () => {
  if (button.children[1].textContent == "Start") {
    startDate = new Date();
    let date = startDate.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
    });
    startTime.innerHTML = date;
    icon.setAttribute("name", "stop-circle");
    button.setAttribute(
      "class",
      "flex justify-center items-center gap-2 px-24 py-4 text-xl bg-red-500 rounded hover:bg-red-600"
    );
    button.children[1].textContent = "Stop";
    button.setAttribute("onclick", "stopTimer()");
  }
};

// stop time
const stopTimer = () => {
  if (button.children[1].textContent == "Stop") {
    let stopDate = new Date();
    let date = stopDate.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
    });
    stopTime.innerHTML = date;

    // calculate money
    let calTime = (stopDate - startDate) / 60000;

    minute.innerHTML = Math.trunc(calTime);
    totalTime(calTime);
    icon.setAttribute("name", "close-circle");
    button.setAttribute(
      "class",
      "flex justify-center items-center gap-2 px-24 py-4 text-xl bg-yellow-500 rounded hover:bg-yellow-600"
    );
    button.children[1].textContent = "Clear";
    button.setAttribute("onclick", "clearTimer()");
  }
};

// clear money and time of user
const clearTimer = () => {
  startTime.innerHTML = "00:00";
  stopTime.innerHTML = "00:00";
  minute.innerHTML = 0;
  currency.innerHTML = 0;
  icon.setAttribute("name", "play-circle");
  button.setAttribute(
    "class",
    "flex justify-center items-center gap-2 px-24 py-4 text-xl bg-lime-500 rounded hover:bg-lime-600"
  );
  button.children[1].textContent = "Start";
  button.setAttribute("onclick", "startTimer()");
};

// calculate to find total of user
function totalTime(calTime) {
  let totalMoney = 1500;
  if (calTime <= 15) {
    currency.innerHTML = 500;
  } else if (calTime <= 30) {
    currency.innerHTML = 1000;
  } else if (calTime <= 60) {
    currency.innerHTML = 1500;
  } else {
    do {
      calTime -= 60;
      if (calTime <= 15) {
        totalMoney += 500;
      } else if (calTime <= 30) {
        totalMoney += 1000;
      } else if (calTime <= 60) {
        totalMoney += 1500;
      } else {
        totalMoney += 1500;
      }
    } while (calTime >= 60);
    currency.innerHTML = totalMoney;
  }
}
